package main

//Each record in miniDB
type Record struct {
	Key    string    `json:"-"`
	Value  string    `json:"value,omitempty"`
	LockId string    `json:"lock_id"`
	Access chan bool `json:"-"`
}

//Collection of all Records
type MiniDB []Record

//Our mini database store
var miniDB MiniDB

//Error type
type Error struct {
	ErrorType string `json:"error"`
}
