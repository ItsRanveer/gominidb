package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

//NewRouter will register all your routes with a logger and handler func
func NewRouter() *mux.Router {

	router := mux.NewRouter()

	//Range through all routes defined in routes.go and create handler for them
	for _, route := range routes {

		//Creating a logger on your Handler
		var handler http.Handler
		handler = route.HandlerFunc
		handler = Logger(handler, route.Name)

		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}

	return router
}
