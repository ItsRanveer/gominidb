package main

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/gorilla/mux"
)

//Method - POST
//URI Pattern - /reservations/{key}
func Reservation(w http.ResponseWriter, r *http.Request) {
	//Get key from URI
	vars := mux.Vars(r)
	key := vars["key"]

	//Get the pointer to record if key present in miniDB
	record := findRecord(key)

	if *record != (Record{}) { //If key present

		var lockedRecord Record
		if record.LockId != "" { //If Record Locked than wait for release & than Lock it
			select {
			case <-record.Access: //Listen on Record's Access channel to get release notification
				lockedRecord = reserve(record)
			}
		} else { //If Record not Locked than than Lock it
			lockedRecord = reserve(record)
		}

		//Once Record is Locked than send Status 200 and in body Record Value and LockId
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(w).Encode(lockedRecord); err != nil {
			panic(err)
		}

	} else { //If key not present than send Not Found
		//Set Status 404 for key not Found
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusNotFound)
		if err := json.NewEncoder(w).Encode(Error{ErrorType: "Key Not Found"}); err != nil {
			panic(err)
		}

	}
}

//Method - POST
//URI Pattern - /values/{key}/{lock_id}?release={true, false}
func UpdateReserved(w http.ResponseWriter, r *http.Request) {
	//Get key and LockId from URI
	vars := mux.Vars(r)
	key := vars["key"]
	lock_id := vars["lock_id"]

	//Get release value from Query String
	parsedQuery, err := url.ParseQuery(r.URL.RawQuery)
	if err != nil {
		panic(err)
	}
	release := parsedQuery["release"][0]

	//Get "value" to set for "key" from request body
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1024)) // 1 Kb Body Length
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	value := string(body[:])

	//Get the pointer to record if key present in miniDB
	record := findRecord(key)

	if *record != (Record{}) { //If key present

		if record.LockId == lock_id { //If lock_id matches with record'd LockId

			if release == "true" { //Set Value and release Lock

				record.Value = value //Set new Value
				select {
				case record.Access <- true:
					record.LockId = "" //Release Lock
				default:
					record.LockId = "" //Release Lock
				}
				//Set Status 204 for No Content
				w.Header().Set("Content-Type", "application/json; charset=UTF-8")
				w.WriteHeader(http.StatusNoContent)

			} else if release == "false" { //Set Value but don't release Lock
				record.Value = value //Set new Value
				//Set Status 204 for No Content
				w.Header().Set("Content-Type", "application/json; charset=UTF-8")
				w.WriteHeader(http.StatusNoContent)
			}

		} else { //If lock_id does not match with record'd LockId than Unauthorized Access
			//Set Status 401 for Unauthorized Access
			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusUnauthorized)
			if err := json.NewEncoder(w).Encode(Error{ErrorType: "Unauthorized Access"}); err != nil {
				panic(err)
			}
		}

	} else { //If key not present than send Not Found
		//Set Status 404 for key not Found
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusNotFound)
		if err := json.NewEncoder(w).Encode(Error{ErrorType: "Key Not Found"}); err != nil {
			panic(err)
		}

	}
}

//Method - PUT
//URI Pattern - /values/{key}
func UpsertAndReserve(w http.ResponseWriter, r *http.Request) {
	//Get key from URI
	vars := mux.Vars(r)
	key := vars["key"]

	//Get "value" to set for "key" from request body
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1024)) // 1 Kb Body Length
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	value := string(body[:])

	//Get the pointer to record if key present in miniDB
	record := findRecord(key)

	if *record != (Record{}) { //If key present

		var lockedRecord Record
		if record.LockId != "" { //If Record Locked than wait for release, update value and than Lock it
			select {
			case <-record.Access:
				lockedRecord = reserveWithUpdatedValue(record, value)
			}
		} else { //If Record not Locked than update Value and Lock it
			lockedRecord = reserveWithUpdatedValue(record, value)
		}
		//Once Record is Locked than send Status 200 and in response body send LockId
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(w).Encode(Record{LockId: lockedRecord.LockId}); err != nil {
			panic(err)
		}

	} else { //If key not present than insert new record and lock it
		//Create a new Record with given key and value and create LockId and chan
		//Than insert it in miniDB
		newRecord := Record{Key: key, Value: value, LockId: randLockIdGenerator(10), Access: make(chan bool)}
		insertInMiniDB(newRecord)

		//Once new Record is inserted and locked than send Status 200 and in response body send LockId
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(w).Encode(Record{LockId: newRecord.LockId}); err != nil {
			panic(err)
		}
	}
}
