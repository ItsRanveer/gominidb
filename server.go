package main

import (
	"log"
	"net/http"
)

//Main entry Point
func main() {
	//Call to NewRouter will register all your routes with a logger and handler func.
	//Return a *mux.Router
	router := NewRouter()

	//Listen and Serve at Port: 8080
	log.Fatal(http.ListenAndServe(":8080", router))
}
