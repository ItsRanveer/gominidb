package main

import (
	"math/rand"
	"time"
)

//UTF-8 for creating random LockId
var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

//Seeding some key value data while initializing
func init() {
	rand.Seed(time.Now().UTC().UnixNano())
	insertInMiniDB(Record{Key: "123", Value: "ABC", Access: make(chan bool)})
	insertInMiniDB(Record{Key: "456", Value: "DEF", Access: make(chan bool)})
	insertInMiniDB(Record{Key: "789", Value: "GHI", Access: make(chan bool)})
}

//This will append new record to miniDB
func insertInMiniDB(record Record) {
	miniDB = append(miniDB, record)
}

//This will create and return a random string of n length
func randLockIdGenerator(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

//This will take the pointer to the record, add a LockId and returns same record
func reserve(record *Record) Record {
	record.LockId = randLockIdGenerator(10)
	return *record
}

//This will take the pointer to the record, add a LockId, update Value
//and returns same record
func reserveWithUpdatedValue(record *Record, value string) Record {
	record.LockId = randLockIdGenerator(10)
	record.Value = value
	return *record
}

//This will range over miniDB and find the pointer to record matching the key passed
func findRecord(key string) *Record {
	for index, record := range miniDB {
		if record.Key == key {
			return &miniDB[index]
		}
	}
	return &Record{}
}
