package main

import "net/http"

//Route type
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

//Collection of all Routes
type Routes []Route

//Define all routes here with proper Name, Method, URI Pattern and HandlerFunc
var routes = Routes{
	Route{
		"Reservation",
		"POST",
		"/reservations/{key}",
		Reservation,
	},
	Route{
		"UpdateReserved",
		"POST",
		"/values/{key}/{lock_id}",
		UpdateReserved,
	},
	Route{
		"Putter",
		"PUT",
		"/values/{key}",
		UpsertAndReserve,
	},
}
